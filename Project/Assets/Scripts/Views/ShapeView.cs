﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeView : MonoBehaviour
{
    public static implicit operator ShapeModel(ShapeView me) {
        return me.model;
    }
    
    public Pool pool;
    public bool isShadow = false;
    BlockView[] blocks;
    ShapeModel model;

    public void Init(ShapeModel model) {
        if(blocks != null) {
            for(int i = 0; i < blocks.Length; ++i) {
                blocks[i].ReturnToPool();
            }
        }
        this.model = model;
        if(model != null) {
            blocks = new BlockView[model.GetBlocksLength()];
            for (int i = 0; i < blocks.Length; ++i) {
                var block = pool.GetBlock();
                block.transform.SetParent(transform);
                block.Init(model.GetBlock(i));
                if (isShadow) block.SetAsShadow();
                else block.SetAsTetris();
                blocks[i] = block;
            }
        } else {
            blocks = null;
        }
    }

    void Update() {
        if (model != null) {
            var pos = model.GetPosition();
            var newPos = new Vector3(pos.x, pos.y, pos.z);
            transform.localPosition = newPos;
        }
    }

    public ShapeModel testModel;
    public Axis testAxis;

    [ContextMenu("Test Rotate")]
    public void TestRotate() {
        model.Rotate(testAxis);
    }
    [ContextMenu("Test Init")]
    public void TestInit() {
        Init(testModel);
    }
    
}
