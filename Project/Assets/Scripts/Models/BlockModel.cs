﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BlockModel 
{
    [SerializeField]
    Vector3 position;
    [SerializeField]
    Color color;
    
    public BlockModel(Vector3 position, Color color) {
        this.position = position;
        this.color = color;
    }

    public BlockModel(BlockModel other) {
        this.position = other.position;
        this.color = other.color;
    }

    public Vector3 GetPosition() {
        return position;
    }

    public void SetPosition(Vector3 pos) {
        position = pos;
    }

    public Color GetColor() {
        return color;
    }

    public void Rotate(Matrix4x4 matrix) {
        position = matrix.MultiplyPoint3x4(position);
        position.x = Mathf.Round(position.x);
        position.y = Mathf.Round(position.y);
        position.z = Mathf.Round(position.z);
    }

    public override string ToString() {
        return position.ToString();
    }
}
