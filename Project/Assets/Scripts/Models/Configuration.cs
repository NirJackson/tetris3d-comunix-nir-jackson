﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Configuration
{
    Vector3 boardSize, startingPos;
    float interval;
    List<ShapeModel> shapes;

    public Configuration(string gameConfig, string shapesConfig) {
        ParseGameConfig(gameConfig);
        ParseShapes(shapesConfig);
    }

    void ParseGameConfig(string gameConfig) {
        if(gameConfig != null) {
            var lines = gameConfig.Split('\n');
            var gameParams = lines[1].Split(',');
            var xyz = gameParams[0].Split(';');
            boardSize = new Vector3(int.Parse(xyz[0]), int.Parse(xyz[1]), int.Parse(xyz[2]));
            xyz = gameParams[1].Split(';');
            startingPos = new Vector3(int.Parse(xyz[0]), int.Parse(xyz[1]), int.Parse(xyz[2]));
            interval = float.Parse(gameParams[2]);
        }
    }

    void ParseShapes(string shapesConfig) {
        shapes = new List<ShapeModel>();
        if (shapesConfig != null) {
            var lines = shapesConfig.Split('\n');
            for(int i = 1; i < lines.Length; ++i) {
                var shapeParams = lines[i].Split(',');
                var weight = int.Parse(shapeParams[0]);
                var rgb = shapeParams[1].Split(';');
                var color = new Color(int.Parse(rgb[0]) / 255f, int.Parse(rgb[1]) / 255f, int.Parse(rgb[2]) / 255f, 1f);
                List<Vector3> blocks = new List<Vector3>();
                for(int j = 2; j < shapeParams.Length; ++j) {
                    if(!string.IsNullOrWhiteSpace(shapeParams[j])) {
                        var xyz = shapeParams[j].Split(';');
                        blocks.Add(new Vector3(int.Parse(xyz[0]), int.Parse(xyz[1]), int.Parse(xyz[2])));
                    }
                }
                shapes.Add(new ShapeModel(blocks.ToArray(), color, weight));
            }
        }
    }

    public Vector3 GetBoardDimentions() {
        return boardSize;
    }

    public Vector3 GetStartingPosition() {
        return startingPos;
    }

    public ShapeModel GetRandomShape() {
        int sum = 0;
        for (int i = 0; i < shapes.Count; ++i) sum += shapes[i].GetWeight();
        var rnd = Mathf.RoundToInt(Random.Range(0, sum));
        for (int i = 0; i < shapes.Count; ++i) {
            rnd -= shapes[i].GetWeight();
            if (rnd < 0) return new ShapeModel(shapes[i]);
        }
        return new ShapeModel(shapes[0]);
    }

    public float GetInterval() {
        return interval;
    }

}
