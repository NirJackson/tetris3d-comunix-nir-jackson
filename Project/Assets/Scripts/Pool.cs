﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public BlockView blockPrototype;
    Stack<BlockView> blockInactive = new Stack<BlockView>();
    

    public BlockView GetBlock() {
        BlockView retVal = null;
        if (blockInactive.Count > 0) {
            retVal = blockInactive.Pop();
        } else {
            retVal = Instantiate(blockPrototype);
        }
        retVal.SetPool(this);
        retVal.transform.SetParent(transform);
        return retVal;
    }

    public void ReturnBlockToPool(BlockView bv) {
        blockInactive.Push(bv);
        bv.transform.SetParent(transform);
    }
}
