﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenuView : MonoBehaviour
{

    public GameObject mainPanel, scoreObj;
    public TMP_Text scoreText;

    public Action onNewGameClicked, onQuitClicked; 

    public void Open() {
        mainPanel.SetActive(true);
        scoreObj.SetActive(false);
    }

    public void Open(int score) {
        mainPanel.SetActive(true);
        scoreObj.SetActive(true);
        scoreText.text = score.ToString();
    }

    public void Close() {
        mainPanel.SetActive(false);
    }

    public void NewGameButtonClicked() {
        onNewGameClicked?.Invoke();
    }

    public void QuitButtonClicked() {
        onQuitClicked?.Invoke();
    }
}
