﻿public enum Axis {
    X,
    Y,
    Z,
    NegX,
    NegY,
    NegZ
}
public enum GameEvent {
    Forward = 1,
    Backwards = -1,
    Left = 2,
    Right = -2,
    RotateX = 3,
    RotateXNeg = -3,
    RotateY = 4,
    RotateYNeg = -4,
    RotateZ = 5,
    RotateZNeg = -5,
    Down = 6,
    Up = -6
}

public enum Direction {
    North,
    South,
    East,
    West
}