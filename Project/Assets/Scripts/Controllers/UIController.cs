﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    static public UIController instance;
    public MainMenuView mainMenu;
    public HUDView hud;

    void Awake() {
        if (instance != null) {
            Destroy(this);
            return;
        }
        instance = this;
        mainMenu.onNewGameClicked += StartNewGame;
        mainMenu.onQuitClicked += QuitGame;
        mainMenu.Open();
    }

    void OnDestroy() {
        mainMenu.onNewGameClicked -= StartNewGame;
        mainMenu.onQuitClicked -= QuitGame;
    }

    public void StartNewGame() {
        GameController.instance.StartNewGame();
        mainMenu.Close();
        hud.Show();
    }

    public void UpdateHUD(int score, int lvl) {
        hud.SetLevel(lvl);
        hud.SetScore(score);
    }

    public void ShowMainMenu(int score = 0) {
        hud.Hide();
        if(score > 0) {
            mainMenu.Open(score);
        } else {
            mainMenu.Open();
        }
    }

    public void QuitGame() {
        Application.Quit();
    }

}
