﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BoardMatrix : IEnumerable<BlockModel>
{
    //Rotation matrixes
    public readonly Matrix4x4 xRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(90, 0, 0));
    public readonly Matrix4x4 yRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 90, 0));
    public readonly Matrix4x4 zRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, 90));


    public ShapeModel activeShapeModel { get; private set; }
    public ShapeModel shadowShapeModel { get; private set; }
    public Action onMatrixChanged, onGameOver, onClearPlane;

    Configuration config;
    BlockModel[,,] matrix;
    Vector3Int size;

    public BoardMatrix(Configuration config) {
        this.config = config;
        var size = this.config.GetBoardDimentions();
        this.size = new Vector3Int(Mathf.FloorToInt(size.x), Mathf.FloorToInt(size.y), Mathf.FloorToInt(size.z));
        matrix = new BlockModel[this.size.x, this.size.y + 3, this.size.z];
    }

    public Vector3Int GetBoardSize() {
        return size;
    }

    public override string ToString() {
        return size.ToString();
    }

    public float Update(GameEvent gameEvent, float timeLeft, int lvl) {
        if(activeShapeModel == null) {
            CreateNewActiveShape();
            return config.GetInterval() / lvl; //TODO: get from config
        }
        switch (gameEvent) {
            case GameEvent.Forward:
                activeShapeModel.Move(Vector3.forward);
                break;
            case GameEvent.Backwards:
                activeShapeModel.Move(Vector3.back);
                break;
            case GameEvent.Left:
                activeShapeModel.Move(Vector3.left);
                break;
            case GameEvent.Right:
                activeShapeModel.Move(Vector3.right);
                break;
            case GameEvent.RotateX:
                activeShapeModel.Rotate(Axis.X);
                shadowShapeModel.Rotate(Axis.X);
                break;
            case GameEvent.RotateXNeg:
                activeShapeModel.Rotate(Axis.NegX);
                shadowShapeModel.Rotate(Axis.NegX);
                break;
            case GameEvent.RotateY:
                activeShapeModel.Rotate(Axis.Y);
                shadowShapeModel.Rotate(Axis.Y);
                break;
            case GameEvent.RotateYNeg:
                activeShapeModel.Rotate(Axis.NegY);
                shadowShapeModel.Rotate(Axis.NegY);
                break;
            case GameEvent.RotateZ:
                activeShapeModel.Rotate(Axis.Z);
                shadowShapeModel.Rotate(Axis.Z);
                break;
            case GameEvent.RotateZNeg:
                activeShapeModel.Rotate(Axis.NegZ);
                shadowShapeModel.Rotate(Axis.NegZ);
                break;
            case GameEvent.Down:
                activeShapeModel.Move(Vector3.down);
                timeLeft = config.GetInterval() / lvl; //TODO: get from config
                break;
            case GameEvent.Up:
                activeShapeModel.Move(Vector3.up);
                List<BlockModel> newBlocks = new List<BlockModel>();
                for(int i =0; i < activeShapeModel.GetBlocksLength(); ++i) {
                    var block = activeShapeModel.GetBlock(i);
                    var p = block.GetPosition() + activeShapeModel.GetPosition();
                    block.SetPosition(p);
                    int x = Mathf.RoundToInt(p.x);
                    int z = Mathf.RoundToInt(p.z);
                    int y = Mathf.RoundToInt(p.y);
                    matrix[x, y, z] = block;
                    newBlocks.Add(block);
                }
                CheckPlane();
                if(!CreateNewActiveShape()){
                    return float.MaxValue;
                } else {
                }
                onMatrixChanged?.Invoke();
                break;
        }

        if (!ValidateShape(activeShapeModel)) {
            return Update((GameEvent)(-(int)gameEvent), timeLeft, lvl);
        }
        CalculateShadowPosition();
        return timeLeft;
    }
    
    bool ValidateShape(ShapeModel shape) {
        foreach(var block in shape) {
            int x = Mathf.RoundToInt(block.x);
            if ((x < 0) || (x >= size.x)) {
                return false;
            }
            int z = Mathf.RoundToInt(block.z);
            if ((z < 0) || (z >= size.z)) {
                return false;
            }
            int y = Mathf.RoundToInt(block.y);
            if (y < 0) return false;
            if (y >= size.y) continue;
            
            if(matrix[x,y,z] != null) {
                return false;
            }
        }
        return true;
    }

    void CheckPlane() {
        for(int y = size.y - 1; y > -1; --y) {
            bool full = true;
            for(int x = 0; x < size.x; ++x) {
                for(int z = 0; z<size.z; ++z) {
                    if(matrix[x,y,z] == null) {
                        full = false;
                        break;
                    }
                }
                if (!full) {
                    break;
                }
            }
            if (full) { 
                for (int yy = y + 1; yy < size.y; ++yy) {
                    for (int x = 0; x < size.x; ++x) {
                        for (int z = 0; z < size.z; ++z) {
                            var block = matrix[x, yy, z];
                            if(block != null) block.SetPosition(new Vector3(x, yy - 1, z));
                            matrix[x, yy - 1, z] = block;
                        }
                    }
                }
                onClearPlane?.Invoke();
            }
        }
    }

    bool CreateNewActiveShape() {
        activeShapeModel = config.GetRandomShape();
        activeShapeModel.SetPosition(config.GetStartingPosition());
        if (!ValidateShape(activeShapeModel)) {
            onGameOver?.Invoke();
            return false;
        }
        shadowShapeModel = new ShapeModel(activeShapeModel);
        CalculateShadowPosition();
        return true;
    }

    void CalculateShadowPosition() {
        for (int y = 0; y < size.y; ++y) {
            shadowShapeModel.SetPosition(new Vector3(activeShapeModel.GetPosition().x, y, activeShapeModel.GetPosition().z));
            if (ValidateShape(shadowShapeModel)) break;
        }
    }

    public IEnumerator<BlockModel> GetEnumerator() {
        for (int y = size.y - 1; y > -1; --y) {
            for (int x = 0; x < size.x; ++x) {
                for (int z = 0; z < size.z; ++z) {
                    if(matrix[x,y,z] != null) {
                        yield return matrix[x, y, z];
                    }
                }
            }
        }
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return GetEnumerator();
    }
    
}
