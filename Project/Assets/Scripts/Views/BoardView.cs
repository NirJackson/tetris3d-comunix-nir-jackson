﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour
{
    public Pool pool;
    public Transform tilesParent, blocksParent;
    List<BlockView> tiles, blocks;
    BoardMatrix model;

    //Clear
    public void Init(BoardMatrix model) {
        if(this.model != null) {
            this.model.onMatrixChanged -= UpdateMatrix;
        }
        if (tiles != null) {
            while (tiles.Count > 0) {
                tiles[tiles.Count - 1].ReturnToPool();
                tiles.RemoveAt(tiles.Count - 1);
            }
        }
        ClearBlocks();
        this.model = model;
        if (this.model != null) {
            this.model.onMatrixChanged += UpdateMatrix;
            var size = this.model.GetBoardSize();
            tiles = new List<BlockView>();
            blocks = new List<BlockView>();
            for (int i = 0; i < size.x; ++i) {
                for (int j = 0; j < size.z; ++j) {
                    var tile = pool.GetBlock();
                    tile.SetAsTile();
                    tile.transform.SetParent(tilesParent);
                    tile.SetPosition(new Vector3(i, -0.5f, j));
                    tiles.Add(tile);
                }
            }
            UpdateMatrix();
        }
    }

    void UpdateMatrix() {
        ClearBlocks();
        foreach (var block in model) {
            var blockView = pool.GetBlock();
            blockView.transform.SetParent(blocksParent);
            blockView.Init(block);
            blockView.SetAsTetris();
            blocks.Add(blockView);
        }
    }

    void ClearBlocks() {
        if (blocks != null) {
            while (blocks.Count > 0) {
                blocks[blocks.Count - 1].ReturnToPool();
                blocks.RemoveAt(blocks.Count - 1);
            }
        }
    }

}
