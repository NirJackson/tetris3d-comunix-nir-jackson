﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockView : MonoBehaviour
{


    [SerializeField]
    BlockModel model = null;
    Pool pool;
    bool active = false;

    public Material tetris, shadow, tile;

    public void Init(BlockModel model) {
        this.model = model;
        SetPosition(model.GetPosition());
        SetAsTetris();
        GetComponent<Renderer>().material.SetColor("_Color", model.GetColor());
        active = true;
    }

    public void SetAsTetris() {
        GetComponent<Renderer>().materials = new Material[] { tetris };
        transform.localScale = Vector3.one;
        GetComponent<Renderer>().material.SetColor("_Color", model.GetColor());
    }

    public void SetAsShadow() {
        GetComponent<Renderer>().materials = new Material[] { shadow };
        transform.localScale = Vector3.one;
    }

    public void SetAsTile() {
        GetComponent<Renderer>().materials = new Material[] { tile };
        transform.localScale = new Vector3(1, 0, 1);
    }

    void Update() {
        if (active && (model != null)) {
            SetPosition(model.GetPosition());
        }
    }

    public void SetPosition(Vector3 pos) {
        Vector3 newPos = new Vector3(pos.x, pos.y, pos.z);
        transform.localPosition = newPos;
    }

    public void SetPool(Pool pool) {
        this.pool = pool;
    }

    public void ReturnToPool() {
        pool.ReturnBlockToPool(this);
        model = null;
    }

    public void ChangeMaterial(Material material) {
        GetComponent<Renderer>().materials = new Material[] { material };
    }
}
