﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUDView : MonoBehaviour
{

    public GameObject mainPanel;
    public TMP_Text scoreText, levelText;

    public void Show() {
        mainPanel.SetActive(true);
    }

    public void Hide() {
        mainPanel.SetActive(false);
    }

    public void SetScore(int score) {
        scoreText.text = score.ToString();
    }

    public void SetLevel(int lvl) {
        levelText.text = lvl.ToString();
    }
}
