﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    //Singleton Controller
    static public GameController instance;

    void Awake() {
        if (instance != null) {
            Destroy(this);
            return;
        }
        instance = this;
        config = new Configuration(gameConfig.text, shapesConfig.text);
    }


    public ShapeView activeShape, shadowShape;
    public BoardView board;

    //Configuration files
    public TextAsset gameConfig, shapesConfig;

    Configuration config;
    BoardMatrix boardMatrix;
    bool activeGame = false;
    float time;
    float updateInteval = 0.1f;
    float updateTime = 0;

    int score = 0, level = 0, planesCleared = 0;
    int basePoints = 100;

    //Movement dictionaries
    Dictionary<GameEvent, GameEvent> north = new Dictionary<GameEvent, GameEvent>() {
        [GameEvent.Forward] = GameEvent.Forward,
        [GameEvent.Backwards] = GameEvent.Backwards,
        [GameEvent.Left] = GameEvent.Left,
        [GameEvent.Right] = GameEvent.Right,
        [GameEvent.RotateX] = GameEvent.RotateX,
        [GameEvent.RotateY] = GameEvent.RotateY,
        [GameEvent.RotateZ] = GameEvent.RotateZ
    };
    Dictionary<GameEvent, GameEvent> south = new Dictionary<GameEvent, GameEvent>() {
        [GameEvent.Forward] = GameEvent.Backwards,
        [GameEvent.Backwards] = GameEvent.Forward,
        [GameEvent.Left] = GameEvent.Right,
        [GameEvent.Right] = GameEvent.Left,
        [GameEvent.RotateX] = GameEvent.RotateXNeg,
        [GameEvent.RotateY] = GameEvent.RotateY,
        [GameEvent.RotateZ] = GameEvent.RotateZNeg
    };
    Dictionary<GameEvent, GameEvent> east = new Dictionary<GameEvent, GameEvent>() {
        [GameEvent.Forward] = GameEvent.Left,
        [GameEvent.Backwards] = GameEvent.Right,
        [GameEvent.Left] = GameEvent.Backwards,
        [GameEvent.Right] = GameEvent.Forward,
        [GameEvent.RotateX] = GameEvent.RotateZNeg,
        [GameEvent.RotateY] = GameEvent.RotateY,
        [GameEvent.RotateZ] = GameEvent.RotateX
    };
    Dictionary<GameEvent, GameEvent> west = new Dictionary<GameEvent, GameEvent>() {
        [GameEvent.Forward] = GameEvent.Right,
        [GameEvent.Backwards] = GameEvent.Left,
        [GameEvent.Left] = GameEvent.Forward,
        [GameEvent.Right] = GameEvent.Backwards,
        [GameEvent.RotateX] = GameEvent.RotateZ,
        [GameEvent.RotateY] = GameEvent.RotateY,
        [GameEvent.RotateZ] = GameEvent.RotateXNeg
    };
    

    void Update() {
        if (activeGame) {
            if(time > 0) {
                time -= Time.deltaTime;
            } else {
                time = boardMatrix.Update(GameEvent.Down,time, level);
            }
            //Init if new shape
            if(boardMatrix.activeShapeModel != (ShapeModel)activeShape) {
                activeShape.Init(boardMatrix.activeShapeModel);
            }
            if(boardMatrix.shadowShapeModel != (ShapeModel)shadowShape) {
                shadowShape.Init(boardMatrix.shadowShapeModel);
            }

            //Calculate movement direction relative to camera
            Dictionary<GameEvent, GameEvent> dir;
            switch (CameraController.instance.dir) {
                case Direction.South:
                    dir = south;
                    break;
                case Direction.East:
                    dir = east;
                    break;
                case Direction.West:
                    dir = west;
                    break;
                default:
                case Direction.North:
                    dir = north;
                    break;
            }
            

            //Input events
            if (Input.GetKeyDown(KeyCode.D)) {
                time = boardMatrix.Update(dir[GameEvent.Right], time, level);
            } else if (Input.GetKeyDown(KeyCode.A)) {
                time = boardMatrix.Update(dir[GameEvent.Left], time, level);
            }
            if (Input.GetKeyDown(KeyCode.W)) {
                time = boardMatrix.Update(dir[GameEvent.Forward], time, level);
            } else if (Input.GetKeyDown(KeyCode.S)) {
                time = boardMatrix.Update(dir[GameEvent.Backwards], time, level);
            }
            if (Input.GetKeyDown(KeyCode.E)) {
                time = boardMatrix.Update(dir[GameEvent.RotateX], time, level);
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                time = boardMatrix.Update(dir[GameEvent.RotateY], time, level);
            }
            if (Input.GetKeyDown(KeyCode.F)) {
                time = boardMatrix.Update(dir[GameEvent.RotateZ], time, level);
            }
            if (updateTime < 0) {
                updateTime = updateInteval;
                if (Input.GetKey(KeyCode.Space)) {
                    time = boardMatrix.Update(GameEvent.Down, time, level);
                }
            }
            updateTime -= Time.deltaTime;
        }
    }

    //initialize board, score and level
    public void StartNewGame() {
        if(boardMatrix != null) {
            boardMatrix.onGameOver -= GameOver;
            boardMatrix.onClearPlane -= PlaneCleared;
        }
        boardMatrix = new BoardMatrix(config);
        boardMatrix.onGameOver += GameOver;
        boardMatrix.onClearPlane += PlaneCleared;
        score = 0;
        level = 1;
        planesCleared = 0;
        UIController.instance.UpdateHUD(score, level);
        board.Init(boardMatrix);
        activeGame = true;
    }

    //Stop game loop, open main menu
    public void GameOver() {
        activeGame = false;
        UIController.instance.ShowMainMenu(score);
    }

    //increase score / level
    public void PlaneCleared() {
        ++planesCleared;
        if(planesCleared > 0) {
            planesCleared = 0;
            ++level;
        }
        score += basePoints * level;
        UIController.instance.UpdateHUD(score, level);
    } 

    void OnDestroy() {
        boardMatrix.onGameOver -= GameOver;
        boardMatrix.onClearPlane -= PlaneCleared;
    }

}
