﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    static public CameraController instance;

    void Awake() {
        if (instance != null) {
            Destroy(this);
            return;
        }
        instance = this;
    }

    public Transform cameraPivot, cameraBase, cameraPoint, cameraTrans;

    public float moveSnap = 1f, roteSnap = 1f, xSensitivity = 1f, ySensitivity = 1f;

    public float topAngle = 20f, bottomAngle = 100f;

    public Direction dir { get; private set; }

    void Update() {
        if (Input.GetAxis("Rotate Camera") > 0) {
            var y = Input.GetAxis("Mouse Y") * ySensitivity;
            if (y > 0) {
                var angle = Vector3.Angle(Vector3.down, cameraPivot.forward);
                if (angle < topAngle) {
                    y = 0;
                }
            }else if(y < 0) {
                var angle = Vector3.Angle(Vector3.up, cameraPivot.forward);
                if (angle < bottomAngle) {
                    y = 0;
                }
            }
            cameraPivot.Rotate(Vector3.right, y, Space.Self);
            var x = Input.GetAxis("Mouse X") * xSensitivity;
            cameraBase.Rotate(Vector3.up, x, Space.World);
        }
        cameraTrans.position = Vector3.Lerp(cameraTrans.position, cameraPoint.position, moveSnap);
        cameraTrans.rotation = Quaternion.Lerp(cameraTrans.rotation, cameraPoint.rotation, roteSnap);

        if(Vector3.Angle(Vector3.forward, cameraBase.forward) < 45) {
            dir = Direction.North;
        } else if (Vector3.Angle(Vector3.right, cameraBase.forward) < 45){
            dir = Direction.West;
        } else if (Vector3.Angle(Vector3.left, cameraBase.forward) < 45) {
            dir = Direction.East;
        } else {
            dir = Direction.South;
        }
    }
}
