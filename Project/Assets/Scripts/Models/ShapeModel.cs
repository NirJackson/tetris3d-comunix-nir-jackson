﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShapeModel: IEnumerable<Vector3>
{
    //Rotation Matrixes
    static readonly Matrix4x4 xRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(90, 0, 0));
    static readonly Matrix4x4 yRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 90, 0));
    static readonly Matrix4x4 zRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, 90));
    static readonly Matrix4x4 xNegRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(-90, 0, 0));
    static readonly Matrix4x4 yNegRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, -90, 0));
    static readonly Matrix4x4 zNegRotMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, -90));

    [SerializeField]
    Vector3 position;
    [SerializeField]
    BlockModel[] blocks;
    int weight;
    Color color;

    public ShapeModel(Vector3[] blocks, Color color, int weight) {
        this.weight = weight;
        this.color = color;
        this.blocks = new BlockModel[blocks.Length];
        for (int i = 0; i < blocks.Length; ++i) {
            this.blocks[i] = new BlockModel(blocks[i], color);
        }
    }   

    //Copy constructor
    public ShapeModel(ShapeModel other) {
        this.weight = other.weight;
        this.color = other.color;
        this.blocks = new BlockModel[other.blocks.Length];
        for(int i = 0; i < other.blocks.Length; ++i) {
            blocks[i] = new BlockModel(other.blocks[i]);
        }
    }
    

    public void Rotate(Axis axis) {
        Matrix4x4 mat;
        switch (axis) {
            case Axis.X:
                mat = xRotMatrix;
                break;
            case Axis.Y:
                mat = yRotMatrix;
                break;
            case Axis.Z:
                mat = zRotMatrix;
                break;
            case Axis.NegX:
                mat = xNegRotMatrix;
                break;
            case Axis.NegY:
                mat = yNegRotMatrix;
                break;
            default:
            case Axis.NegZ:
                mat = zNegRotMatrix;
                break;
        }
        for(int i = 0; i < blocks.Length; ++i) {
            blocks[i].Rotate(mat);
        }
    }

    public BlockModel GetBlock(int i) {
        return blocks[i];
    }

    public int GetBlocksLength() {
        return blocks.Length;
    }

    public Vector3 GetPosition() {
        return position;
    }

    public void SetPosition(Vector3 pos) {
        position = pos;
    }

    public int GetWeight() {
        return weight;
    }

    public void Move(Vector3 delta) {
        position += delta;
    }

    public override string ToString() {
        string retVal = "";
        retVal += "Weight: " + weight.ToString() + " Color: " + color + " Blocks: ";
        foreach(var block in blocks) {
            retVal += block.ToString() + ", ";
        }
        return retVal;
    }

    public IEnumerator<Vector3> GetEnumerator() {
        foreach(var block in blocks) {
            yield return block.GetPosition() + position;
        }
    }

    IEnumerator IEnumerable.GetEnumerator() {
        return GetEnumerator();
    }
}
